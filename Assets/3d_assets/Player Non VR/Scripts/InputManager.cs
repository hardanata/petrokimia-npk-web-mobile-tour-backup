using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    ThirdPersonControls ThirdpersonControls;
    PlayerLocomotion playerLocomotion;
    AnimatorManager animatorManager;
    public Vector2 movementInput;
    public Vector2 cameraInput;
    public float cameraInputX;
    public float cameraInputY;

    public float moveAmount;
    public float verticalInput;
    public float horizontalInput;

    public bool b_Input;

    private void Awake()
    {
        animatorManager = GetComponent<AnimatorManager>();
        playerLocomotion = GetComponent<PlayerLocomotion>();
    }


    private void OnEnable()
    {
        if (ThirdpersonControls == null)
        {
            ThirdpersonControls = new ThirdPersonControls();

            ThirdpersonControls.PlayerMovement.Movement.performed += i => movementInput = i.ReadValue<Vector2>();
            ThirdpersonControls.PlayerMovement.Camera.performed += i => cameraInput = i.ReadValue<Vector2>();

            ThirdpersonControls.PlayerAction.B.performed += i => b_Input = true;
            ThirdpersonControls.PlayerAction.B.canceled += i => b_Input = false;


        }
        ThirdpersonControls.Enable();
    }
    private void OnDisable()
    {
        ThirdpersonControls.Disable();
    }

    public void HandleAllInputs()
    {
        HandleMovementInput();
        HandleSprintingInput();
    }

    private void HandleMovementInput()
    {
        verticalInput = movementInput.y;
        horizontalInput = movementInput.x;
        cameraInputX = cameraInput.x;
        cameraInputY = cameraInput.y;

        moveAmount = Mathf.Clamp01(Mathf.Abs(horizontalInput) + Mathf.Abs(verticalInput));
        animatorManager.UpdateAnimatorValues(0, moveAmount, playerLocomotion.isSprinting);
    }

    private void HandleSprintingInput()
    {
        if(b_Input && moveAmount > 0.5f){
            playerLocomotion.isSprinting = true;
        }
        else{
            playerLocomotion.isSprinting = false;
        }
    }
}
